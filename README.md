# Swffty - Convert Flash Animations to Apps

This is a Python3 program that wraps SWF files into either EXEs or Mac apps. A lot of the logic of this program was created by junebeetle over at http://junebeetle.github.io/projector/. This program is a CLI based program that allows for batch converting entire folders of SWFs.
## How to Install
### From source
1. Download the latest version of the source code.
2. Navigate to the folder `src/build` and run the script(s) that correspond to your operating system.
3. Follow all the instructions in the script.



