#!/bin/bash
cd ../..
echo "Make sure to download Python3 from python.org before building (tested to work on 3.7.3)"
echo "You can get the version specified above from https://www.python.org/ftp/python/3.7.3/python-3.7.3-macosx10.9.pkg"
read -r -p "I have installed Python3 and I am ready to continue (y/n): " ready
if [[ "$ready" =~ ^([yY][eE][sS]|[yY])$ ]] 
then
    python3.7 -m venv venv || python3 -m venv venv || python -m venv venv
    source venv/bin/activate
    pip install -r requirements/base.txt
    pip install -r requirements/mac.txt
    cp requirements/path.py venv/lib/python3.7/site-packages/jaraco/path.py
    fbs freeze
else
    echo Exiting
fi