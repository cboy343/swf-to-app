@echo off
echo Make sure to install Python3.7 from python.org before continuing.
set /p installed=I have installed Python3.7 and want to continue (y/n): 
if not %installed%==y goto eof
echo Install the Windows 10 SDK before building
echo The SDK can be found at https://go.microsoft.com/fwlink/p/?linkid=2120843
set /p choice=I have installed the Windows 10 SDK (y/n): 
if %choice%==y goto start_program
goto eof


:start_program
set /p sdk=Type in the path of where you installed the SDK (Default: C:\Program Files (x86)\Windows Kits\10): 
if "%sdk%"=="%sdk%" set sdk=C:\Program Files (x86)\Windows Kits\10\
if exist %sdk% set PATH=%PATH%;%sdk%
cd ../..
python -m venv venv
venv\Scripts\activate
pip install -r requirements\base.txt
pip install -r requirements\windows.txt
copy /Y requirements\bindepend.py venv\Lib\site-packages\PyInstaller\depend\bindepend.py
python -m fbs freeze


:eof