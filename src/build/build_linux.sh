#!/bin/bash
cd ../..
echo "Make sure to download Python3 from your package manager before building (tested to work on 3.7.5)"
read -r -p "I have installed Python3 and I am ready to continue (y/n): " ready
if [[ "$ready" =~ ^([yY][eE][sS]|[yY])$ ]] 
then
    python3.7 -m venv venv || python3 -m venv venv || python -m venv venv
    source venv/bin/activate
    pip install -r requirements/base.txt
    fbs freeze
else
    echo Exiting
fi
