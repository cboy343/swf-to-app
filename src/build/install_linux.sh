#!/bin/bash
if [[ EUID -ne 0 ]]; then
    echo 'You need to run this script with sudo privileges'
    sudo '$0' '$@'
    exit 0
fi
cd ../..
source venv/bin/activate
other_linux() {
    desktop="[Desktop Entry]
    Name=Swffty
    Type=Application
    Exec=/opt/Swffty/Swffty
    Terminal=false
    NoDisplay=false
    Categories=Utility;Development;
    Icon=Swffty
    "
    echo "$desktop" > Swffty.desktop
    cp -r target/Swffty /opt/Swffty
    cp Swffty.desktop /usr/share/applications/Swffty.desktop
    exit 0
}

pkg_man_update="apt-get update"||"dnf update"||"yum update"
pkg_man_install="apt-get install -y ruby ruby-dev rubygems build-essential"||"dnf install -y ruby-devel gcc make rpm-build libffi-devel"||"yum install -y ruby-devel gcc make rpm-build rubygems"||"other_linux"

${pkg_man_update}

${pkg_man_install}

gem install --no-document fpm

fbs installer
cd target
install_command="dpkg -i Swffty.deb"||"rpm -i Swffty.rpm"