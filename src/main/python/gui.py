""" This file contains the logic for the GUI"""
from PyQt5 import uic, QtCore
from PyQt5.QtGui import QIcon, QPalette, QColor, QFont, QFontDatabase
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QVBoxLayout, QLabel, QListWidget, QTextEdit, QFileDialog, QInputDialog, QLineEdit, QListView, QErrorMessage, QListWidgetItem, QCheckBox, QAbstractButton, QProgressBar, QMessageBox, QAction, QDialog, QFileIconProvider, QMainWindow
from advanced_options_dialog import Ui_Dialog as AdvancedDialog
from about_dialog import Ui_Dialog as AboutDialog
from jaraco.path import is_hidden
from pathlib import Path
from platform import release, mac_ver
from os import listdir, scandir, path
from os.path import isdir
from swf_to_app import Convert
from sys import exit

class Gui(QMainWindow):
    """This class has the GUI logic in it"""
    def __init__(self, ui, parent=None):
        """Initiates Gui"""
        super().__init__(parent)
        Form, Window = uic.loadUiType(ui)
        self.convert_class = Convert()
        self.app = QApplication([])
        self.app.setStyle("Fusion")
        self.window = Window()
        self.form = Form()
        self.form.setupUi(self.window)
        QtCore.QResource.registerResource('font.rcc')
        font = QFont("Arial", 16, QFont.StyleNormal)
        self.app.setFont(font)
        self.progressbar = self.window.findChild(QProgressBar, 'progress_bar')
        self.progressbar.setMinimum(0)
        self.progressbar.setMaximum(100)
        self.progressbar_item_percentage = 0

        self.error_message = QErrorMessage()
        self.message = QMessageBox()
        self.raw_items = dict()

        self.path_stack = [self.convert_class.path]
        
        self.flash_browse = self.window.findChild(QPushButton, 'flash_browse')
        self.flash_browse.clicked.connect(self.get_flash)
        self.populate_directory_viewer(self.convert_class.path)
        
        self.download_players = self.window.findChild(QLabel, 'label_3')
        self.download_players.setOpenExternalLinks(True)

        self.path_go_button = self.window.findChild(QPushButton, 'path_go_button')
        self.path_go_button.clicked.connect(lambda: self.change_directory('go'))

        self.path_up_button = self.window.findChild(QPushButton, 'path_up_button')
        self.path_up_button.clicked.connect(lambda: self.change_directory('up'))

        self.path = self.window.findChild(QLineEdit, 'path')
        self.path.setText(self.convert_class.path)
        self.path.returnPressed.connect(lambda: self.change_directory('go'))

        directory_list_box = self.window.findChild(QListWidget, 'directory_view')
        directory_list_box.setSelectionMode(QListWidget.ExtendedSelection)
        directory_list_box.itemDoubleClicked.connect(lambda: self.change_directory(directory_list_box.currentItem().text()))

        selected_items_directory = self.window.findChild(QListWidget, 'selected_items_directory')
        selected_items_directory.setSelectionMode(QListWidget.ExtendedSelection)

        destination_browse = self.window.findChild(QPushButton, 'destination_browse')
        destination_browse.clicked.connect(self.browse_destination)
        # advanced_dialog = AdvancedDialog()
        self.actionAdvanced = self.window.findChild(QAction, 'actionAdvanced')
        self.actionAdvanced.triggered.connect(self.advanced_dialog)
        
        self.actionExit = self.window.findChild(QAction, 'actionExit')
        self.actionExit.triggered.connect(exit)

        self.actionAbout = self.window.findChild(QAction, 'actionAbout')
        self.actionAbout.triggered.connect(self.about_dialog)
        
        add_to_conversion = self.window.findChild(QPushButton, 'add_item_button')
        add_to_conversion.clicked.connect(self.add_item_with_path)
    
        clear_items_button = self.window.findChild(QPushButton, 'clear_items_button')
        clear_items_button.clicked.connect(self.clear_conversion_list)

        remove_item_button = self.window.findChild(QPushButton, 'remove_item_button')
        remove_item_button.clicked.connect(self.remove_item_conversion_list)

        go_button = self.window.findChild(QPushButton, 'start_program_button')
        go_button.clicked.connect(self.start_program)

        is_new_mac = False
        if self.convert_class.os == 'darwin':
            macos_version = mac_ver()[0]
            macos_version = int(macos_version.split('.')[1])
            if macos_version >= 14:
                is_new_mac = True

        is_new_windows = False

        if self.convert_class.os == "win32" and release() == "10":
            from sys import getwindowsversion
            winver = int(getwindowsversion()[2])
            if winver >= 14393:
                is_new_windows = True

        self.check_dark_theme(is_new_mac, is_new_windows)

        self.window.show()
        self.app.exec_()

    def check_dark_theme(self, new_mac, is_new_windows):
        """ Checks if global dark theme is set """
        if is_new_windows:
            settings = QtCore.QSettings("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Themes\\Personalize", QtCore.QSettings.NativeFormat)
            if settings.value("AppsUseLightTheme") == 0:
                self.set_dark_theme()
        elif new_mac:
            global_settings = QtCore.QSettings("/Library/Preferences/.GlobalPreferences.plist")
            if global_settings.value("AppleInterfaceStyle") == "Dark":
                self.set_dark_theme()

    def set_dark_theme(self):
        """Sets app style to dark theme"""
        palette = QPalette()
        main_colour = QColor(45,45,45)
        disabled_colour = QColor(127,127,127)
        palette.setColor(QPalette.Window, main_colour)
        palette.setColor(QPalette.WindowText, QColor(255,255,255))
        palette.setColor(QPalette.Base, QColor(18,18,18))
        palette.setColor(QPalette.AlternateBase, main_colour)
        palette.setColor(QPalette.ToolTipBase, QColor(255,255,255))
        palette.setColor(QPalette.ToolTipText, QColor(255,255,255))
        palette.setColor(QPalette.Text, QColor(255,255,255))
        palette.setColor(QPalette.Button, main_colour)
        palette.setColor(QPalette.ButtonText, QColor(255,255,255))
        palette.setColor(QPalette.Disabled, QPalette.Text, disabled_colour)
        palette.setColor(QPalette.Disabled, QPalette.ButtonText, disabled_colour)
        palette.setColor(QPalette.BrightText, QColor(200,0,0))
        palette.setColor(QPalette.Link, QColor(42,130,218))
        palette.setColor(QPalette.Highlight, QColor(42,130,218))
        palette.setColor(QPalette.HighlightedText, QColor(255,255,255))
        palette.setColor(QPalette.Disabled, QPalette.HighlightedText, disabled_colour)
        
        QApplication.setPalette(palette)

    def about_dialog(self):
        """Opens up the About Dialog"""
        Dialog = QDialog()
        ui = AboutDialog()
        ui.setupUi(Dialog)
        Dialog.show()
        Dialog.exec_()

    def advanced_dialog(self):
        """Opens advanced dialog box"""
        hidden_state_changed = self.convert_class.hidden_files
        Dialog = QDialog()
        ui = AdvancedDialog()
        ui.setupUi(Dialog, self.convert_class)
        Dialog.show()
        accepted = Dialog.exec_()

        if accepted == Dialog.Accepted:
            flash_text = self.window.findChild(QLineEdit, 'flash_select_text')
            flash_text.setText("")
            if hidden_state_changed != self.convert_class.hidden_files:
                self.populate_directory_viewer(self.convert_class.path)

    def exclude_hidden_folders(self, file):
        """Excludes hidden folders if self.convert_class.hidden_files is False"""
        hidden = is_hidden(file)
        if self.convert_class.hidden_files == True:
            return True
        elif self.convert_class.hidden_files == False and not hidden:
            return True
        else:
            return False

    def add_item_with_path(self):
        """Adds an item with a path"""
        directory_list_box = self.window.findChild(QListWidget, 'directory_view')
        if len(directory_list_box.selectedItems()) > 0:
            for list_item in directory_list_box.selectedItems():
                selected_item = list_item.text()
                convert_list = self.window.findChild(QListWidget, 'selected_items_directory')
                item_path = self.convert_class.slash_checker([self.convert_class.path])[0] + selected_item
                any_similar_items = convert_list.findItems(selected_item, QtCore.Qt.MatchExactly)
                # print(any_similar_items)
                if len(any_similar_items) > 0:
                    check_if_added = self.convert_class.slash_checker([self.convert_class.path])[0] + selected_item
                    # print(check_if_added, directory_list_box.selectedItems()[0].data(QtCore.Qt.UserRole + 1))
                    all_items = convert_list.findItems('*', QtCore.Qt.MatchWildcard)
                    added_items = [item.data(QtCore.Qt.UserRole + 1) for item in all_items]
                    if check_if_added in added_items:
                        self.error_message.showMessage("You've already added this item")
                        return
                    else:
                        how_many = len(any_similar_items)
                        # print(len(any_similar_items))
                        is_dir = isdir(item_path)
                        if is_dir:
                            selected_item = '{} ({})'.format(selected_item, how_many)
                        else:
                            selected_item = '{} ({}){}'.format(selected_item[:-4], how_many, selected_item[-4:])
                
                # widget_item = QListWidgetItem()
                # path = QtCore.Qt.UserRole + 1
                # print('path: {}, item_path: {}'.format(path, item_path))
                # widget_item
                is_dir = isdir(item_path)
                # print(is_dir)
                self.raw_items[item_path] = {'name': selected_item, 'isdir': is_dir, 'subdir': self.convert_class.path}
                # print(self.raw_items)
                widget_item = self.add_image_to_file(item_path, selected_item, True)
                convert_list.addItem(widget_item)

    def dump(self, obj): # Thanks to Community♦ and stacker for this cool debugging function https://blender.stackexchange.com/questions/1879/is-it-possible-to-dump-an-objects-properties-and-methods

        #This is a debugging tool that lists all of the methods and attributes that a class has
        #For every method of that class:
        for attr in dir(obj):
            if hasattr( obj, attr ):
                #print the attributes of that method
                print( "obj.%s = %s" % (attr, getattr(obj, attr)))

    def start_program(self):
        """ This method starts the conversion process """
        ready_list = [self.raw_items, self.convert_class.flash_exe, self.convert_class.destination]
        ready = True
        for item in ready_list:
            if len(item) == 0:
                ready = False
        if ready:
            recursive_checkbox = self.window.findChild(QAbstractButton, 'recursive_checkbox')
            recursive = recursive_checkbox.isChecked()
            # print('recursive: {}'.format(recursive))
            for swf_path, swf_details in self.raw_items.items():
                if swf_details['isdir']:
                    success = self.convert_class.get_swfs_in_folder(swf_path, recursive)
                else:
                    self.convert_class.swfs[swf_path] = [swf_details['subdir'], swf_details['name']]
                    success = 'success'
                    
                if success != 'success':
                    self.error_message.showMessage(success)
                    return
            self.progressbar_item_percentage = 100 / len(self.convert_class.swfs)
            if self.convert_class.target_os == 'darwin':
                message = self.convert_class.mac_copy(self.convert_class.swfs, self.convert_class.destination, self.convert_class.flash_exe, self.progressbar, self.progressbar_item_percentage)
            else:
                message = self.convert_class.adobe_reader(self.convert_class.swfs, self.convert_class.flash_exe, self.convert_class.destination, self.progressbar, self.progressbar_item_percentage)
            
            if message[:5] == "ERROR":
                self.error_message.showMessage(message)
            else:
                self.message.setWindowTitle("Success!")
                self.message.setText(message)
                self.message.show()
                self.progressbar.setValue(0)
                recursive_checkbox.setChecked(False)
            # self.raw_items.clear()
            # self.convert_class.swfs.clear()
            self.convert_class.flash_exe, self.convert_class.destination = str(), str()

            flash_text = self.window.findChild(QLineEdit, 'flash_select_text')
            flash_text.setText("")
            destination_text = self.window.findChild(QLineEdit, 'lineEdit')
            destination_text.setText("")
            self.clear_conversion_list()

        else:
            self.error_message.showMessage("Please fill in all the neccesary fields and try again")

        # print(item.text(), item.data(QtCore.Qt.UserRole + 1))

    def browse_destination(self):
        """Get the destination for the swfs"""
        destination = QFileDialog().getExistingDirectory(None, 'Select a destination folder')
        if destination:
            self.convert_class.destination = self.convert_class.slash_checker([destination])[0]
            destination_text = self.window.findChild(QLineEdit, 'lineEdit')
            destination_text.setText(destination)

    def clear_conversion_list(self):
        """Clears the conversion list"""
        convert_list = self.window.findChild(QListWidget, 'selected_items_directory')
        self.convert_class.swfs.clear()
        self.raw_items.clear()
        convert_list.clear()

    def remove_item_conversion_list(self):
        """Removes an item from the conversion list"""
        convert_list = self.window.findChild(QListWidget, 'selected_items_directory')
        if len(convert_list.selectedItems()) > 0:
            for list_item in convert_list.selectedItems():   
                item_path = list_item.data(QtCore.Qt.UserRole + 1)
                del(self.raw_items[item_path])
                # print(self.raw_items)
                convert_list.takeItem(convert_list.row(list_item))



    def change_directory(self, type):
        """This method changes the viewed directory when the user wants to change current path"""
        typed_path = self.window.findChild(QLineEdit, 'path')
        if type == 'go':
            path_text = typed_path.text()
            if not Path(path_text).exists():
                self.error_message.showMessage("Please check the path you typed in")
            else:
                self.convert_class.path = path_text
                self.populate_directory_viewer(path_text)
        elif type == 'up':
            # print(Path(self.convert_class.path).parent)
            self.convert_class.path = str(Path(self.convert_class.path).parent)
            
            self.populate_directory_viewer(self.convert_class.path)
        
        else:
            new_directory = self.convert_class.slash_checker([self.convert_class.path])[0] + type
            if not Path(new_directory).exists() and path.isdir(new_directory):
                self.error_message.showMessage("Hmm. This folder doesn't seem to exist anymore. Try again. ")
            elif Path(new_directory).exists() and path.isdir(new_directory):
                self.convert_class.path = new_directory
                # typed_path.setText(self.convert_class.path)
                self.populate_directory_viewer(self.convert_class.path)                
            
    def add_image_to_file(self, file_path, file_name, data=None):
        """Adds the file image to the file in directory view"""
        file_info = QtCore.QFileInfo(file_path)
        icon_provider = QFileIconProvider()
        file_icon = icon_provider.icon(file_info)
        file_with_icon = QListWidgetItem()
        file_with_icon.setText(file_name)
        file_with_icon.setIcon(file_icon)
        qt_role = QtCore.Qt.UserRole + 1
        if data:
            file_with_icon.setData(qt_role, file_path)
        return file_with_icon

    def populate_directory_viewer(self, directory):
        """This shows all the files in the current directory, except for the ones selected for converting"""
        typed_path = self.window.findChild(QLineEdit, 'path')
        try:
            files = list()
            for file in listdir(directory):
                full_path = self.convert_class.slash_checker([directory])[0] + file
                if file.endswith('.swf'):
                    file = self.add_image_to_file(full_path, file)
                    files.append(file)
                elif path.isdir(full_path):
                    can_add_folder = self.exclude_hidden_folders(full_path)
                    if can_add_folder:
                        if not file.endswith('.app'):
                            file = self.add_image_to_file(full_path, file)
                            files.append(file)
            files = sorted(files, key=lambda x: x.text().lower())
        except PermissionError:
            self.error_message.showMessage("Oops, you can't access this folder. Try a different folder.")
            self.convert_class.path = self.path_stack[0]
            return
        self.path_stack.insert(0, self.convert_class.path)
        if len(self.path_stack) > 10:
            self.path_stack.pop()

        typed_path.setText(self.convert_class.path)
        directory_list_box = self.window.findChild(QListWidget, 'directory_view')
        directory_list_box.clear()
        # print(files)
        for file in files:
            directory_list_box.addItem(file)

    def get_flash(self):
        """This opens the file picker to select the flash application"""
        if self.convert_class.target_os == 'darwin':
            if self.convert_class.os != 'darwin':
                flash = QFileDialog().getExistingDirectory(None, 'Open Flash App')
                self.convert_class.flash_exe = flash
            else:
                flash = QFileDialog().getOpenFileName(None, "Open Flash App", '', 'Mac App (*.app)')
                self.convert_class.flash_exe, _ = flash 
        else:
            flash = QFileDialog().getOpenFileName(None, "Open Flash App", '', 'Windows App (*.exe)')
            self.convert_class.flash_exe, _ = flash 
        filename = Path(self.convert_class.flash_exe).name
        flash_text = self.window.findChild(QLineEdit, 'flash_select_text')
        flash_text.setText(filename)
