""" This program batch produces exes/mac apps from swfs. Created by Cameron Mayes, but heavily borrows from http://junebeetle.github.io/projector/"""
from os import listdir, makedirs, walk
from os.path import expandvars, expanduser, isdir
from shutil import copy, copytree
from sys import platform

class Convert():
    """Logic for program"""
    def __init__(self):
        """Initialises class"""
        self.path = expanduser('~')
        default_os_lambda = lambda x: 'win32' if (x == 'linux') else x
        self.target_os = default_os_lambda(platform)
        self.os = platform
        self.hidden_files = False
        self.flash_exe = str()
        self.destination = str()
        self.swfs = dict()

    def adobe_reader(self, swf_dict, flash, directory, progressbar, progress_percentage):
        """ Duplicates Flash executable and turns it into a projector """
        try:
            with open(flash,'rb') as byte_stream:
                exe_data = byte_stream.read()
        except (OSError, IOError) as error:
            return 'ERROR: Failed to read Adobe executable. Code: %s' % error
            # sys.exit()
        current_index = 0
        for swf_path, swf_details in swf_dict.items():
            output = bytearray()
            output += exe_data
            # swf_file = swf_list[i][0] + swf_list[i][1]
            try:
                with open(swf_path, 'rb') as swf_stream:
                    swf_binary = swf_stream.read()
            except (OSError, IOError) as error:
                return 'ERROR: Failed to read {} swf file. Code: {}'.format(swf_details[1][:-4], error)
            output += swf_binary

            end_data = bytearray()
            end_data.append(0x56)
            end_data.append(0x34)
            end_data.append(0x12)
            end_data.append(0xFA)

            swf_data_length = len(swf_binary)
            while swf_data_length > 0:           
                byte = swf_data_length % 0x100
                end_data.append(byte)
                swf_data_length = swf_data_length // 0x100
            
            while len(end_data) < 8:
                end_data.append(0x00)

            output.extend(end_data)
            swf_details[1] = self.check_for_existing_swf(directory, swf_details[1])
            file_destination = directory + swf_details[1][:-4] + '.exe'
            # print(file_destination)
            try:
                with open(file_destination, 'wb') as exe_file:
                    exe_file.write(output)
                current_index += 1
                progressbar.setValue(progress_percentage * current_index)
            except (OSError, IOError) as error:
                return 'ERROR: Unable to write {} file. Code: {}'.format(swf_details[1], error)
        num_swfs = len(swf_dict)
        return "Successfully created {} files from Flash Animations!".format(num_swfs)

    def check_for_existing_swf(self, destination, swf_name):
        """Checks if folder has file by the same name to avoid overwriting"""
        file_ext = {'darwin': '.app', 'win32': '.exe'}[self.target_os]
        swf_name = swf_name[:-4]
        unique = False
        number_diff = 0
        whole_file = '{}{}'.format(swf_name, file_ext)
        while not unique:
            directory = listdir(destination)
            number_diff += 1
            
            if whole_file in directory:
                # selected_item = '{} ({}){}'.format(selected_item[:-4], how_many, selected_item[-4:])
                whole_file = '{} ({}){}'.format(swf_name, number_diff, file_ext)
            else:
                unique = True
                whole_file = whole_file[:-4] + '.swf'
        return whole_file

        
    def get_swfs_in_folder(self, swfs, recursive):
        """Gets all the swfs in a folder (or folders if program is set to recurse)"""
        # swf_list = dict()

        # swf_paths = swfs.keys()
        if recursive:
            # increment = 0
            for subdir, _, files in walk(swfs):
                try:
                    swf_files = [file for file in files if file.endswith('.swf')]
                except PermissionError:
                    return "ERROR: You don't have access to folder at '{}'. Try changing what folders you've selected.".format(subdir)
                current_subdirectory = subdir
                for i in range(len(swf_files)):
                    current_subdirectory = self.slash_checker([current_subdirectory])
                    current_subdirectory = current_subdirectory[0]
                    full_path = current_subdirectory + swf_files[i]
                    # occurences = [path for path in swf_paths if path.endswith(swf_files[i])] #fix this
                    self.swfs[full_path] = [current_subdirectory, swf_files[i]]
                    # increment += 1
        else:
            try:
                files = [file for file in listdir(swfs) if file.endswith('.swf')]
            except PermissionError:
                return "ERROR: Oops, you don't have access to the folder called '{}'. Try again with a different folder.".format(swfs)
            current_folder = self.slash_checker([swfs])[0]
            
            for i in range(len(files)):
                full_path = current_folder + files[i]
                self.swfs[full_path] = [swfs, files[i]]

        if len(self.swfs) == 0:
            return 'ERROR: No Flash animations found.'
            # sys.exit()
        
        return 'success'

    def mac_copy(self, swf_dict, destination, flash, progressbar, progress_percentage):
        """ Copies Adobe app and copies swf into it """
        # swf_list = self.get_swfs_in_folder(swfs, recursive)
        current_index = 0
        for swf_path, swf_details in swf_dict.items():
            swf_details[1] = self.check_for_existing_swf(destination, swf_details[1])
            destination_path = destination + swf_details[1][:-4] + '.app'
            try:
                copytree(flash, destination_path)
            except (OSError, IOError) as error:
                return 'ERROR: Failed to copy Adobe app. Code: %s' % error
            swf_destination = destination_path + '/Contents/Resources/movie.swf'
            try:
                copy(swf_path, swf_destination)
            except (OSError, IOError) as error:
                return 'ERROR: Unable to copy {} swf file. Code: {}'.format(swf_details[1], error)
            current_index += 1
            progressbar.setValue(progress_percentage * current_index)
        num_swfs = len(swf_dict)
        return 'Successfully created {} Mac apps from Flash Animations!'.format(num_swfs)

    def slash_checker(self, directories):
        """Checks if platform appropriate slash is on the end of directory name"""
        final_directories = list()
        for folder in directories:
            if folder[-1] != '/' and folder[-1] != '\\':
                if platform == 'win32':
                    folder += '\\'
                else:
                    folder += '/'
                final_directories.append(folder)
            else:
                final_directories.append(folder)
        return final_directories