# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Advanced Options Dialog.ui'
#
# Created by: PyQt5 UI code generator 5.12.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from sys import exit

class Ui_Dialog(object):
    def setupUi(self, Dialog, convert_class):
        self.convert_class = convert_class
        Dialog.setObjectName("Dialog")
        Dialog.setWindowModality(QtCore.Qt.ApplicationModal)
        Dialog.resize(411, 295)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(Dialog.sizePolicy().hasHeightForWidth())
        Dialog.setSizePolicy(sizePolicy)
        Dialog.setMinimumSize(QtCore.QSize(411, 295))
        Dialog.setMaximumSize(QtCore.QSize(411, 295))
        # font = QtGui.QFont()
        # font.setFamily("Arial")
        # font.setPointSize(16)
        # Dialog.setFont(font)
        Dialog.setModal(True)
        self.buttonBox = QtWidgets.QDialogButtonBox(Dialog)
        self.buttonBox.setGeometry(QtCore.QRect(30, 240, 341, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.formLayoutWidget = QtWidgets.QWidget(Dialog)
        self.formLayoutWidget.setGeometry(QtCore.QRect(30, 100, 371, 92))
        self.formLayoutWidget.setObjectName("formLayoutWidget")
        self.gridLayout = QtWidgets.QGridLayout(self.formLayoutWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.gridLayout.setVerticalSpacing(5)
        self.label = QtWidgets.QLabel(self.formLayoutWidget)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 1, 0, 1, 1)
        self.comboBox = QtWidgets.QComboBox(self.formLayoutWidget)
        self.comboBox.setObjectName("comboBox")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.setMinimumHeight(25)
        self.gridLayout.addWidget(self.comboBox, 2, 0, 1, 1)
        self.hidden_checkbox = QtWidgets.QCheckBox(self.formLayoutWidget)
        self.hidden_checkbox.setObjectName("hidden_checkbox")
        self.hidden_checkbox.setText("Show hidden files and folders")
        self.hidden_checkbox.setChecked(self.convert_class.hidden_files)
        self.gridLayout.addWidget(self.hidden_checkbox, 3, 0, 1, 1)

        self.retranslateUi(Dialog)
        self.buttonBox.accepted.connect(Dialog.accept)
        self.buttonBox.rejected.connect(Dialog.reject)
        self.buttonBox.accepted.connect(self.yes_button_clicked)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def yes_button_clicked(self):
        """Makes the changes the user has specified"""
        choice = self.comboBox.currentText()
        choice_dict = {'Windows': 'win32', 'Mac OS': 'darwin'}
        self.convert_class.target_os = choice_dict[choice]
        hidden_checked = self.hidden_checkbox.isChecked()
        if hidden_checked:
            self.convert_class.hidden_files = True
        else:
            self.convert_class.hidden_files = False

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Swffty - Advanced"))
        self.label.setText(_translate("Dialog", "Select an OS for your Flash Apps"))
        self.comboBox.setItemText(0, _translate("Dialog", "Windows"))
        self.comboBox.setItemText(1, _translate("Dialog", "Mac OS"))
        default_platform_dict = {'darwin': 1, 'win32': 0}
        self.comboBox.setCurrentIndex(default_platform_dict[self.convert_class.target_os])


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    exit(app.exec_())
